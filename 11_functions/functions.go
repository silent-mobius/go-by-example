package main

import "fmt"

func plus(a int, b int) int {
	return a + b
}

func plusPlus(a, b, c int) int {
	return a + b + c
}

func main() {

	res := plus(3, 5)
	//fmt.Println("1+2 =", res) # original from the site: gobyexample.com
	fmt.Printf("%d + %d = %d\n", 3, 5, res)

	res = plusPlus(4, 7, 9)
	//fmt.Println("1+2+3 =", res) # original from the site
	fmt.Printf("%d + %d + %d = %d", 4, 7, 9, res)
}
