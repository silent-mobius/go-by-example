package main

import "fmt"

func vals() (int, int) {
	return 3, 5
}

func main() {

	a, b := vals()
	// fmt.Println(a) # oriinal from site
	// fmt.Println(b)
	fmt.Printf("%d\n", a)
	fmt.Printf("%d\n", b)

	_, c := vals()
	// fmt.Println(c)
	fmt.Printf("%d\n", c)

}
