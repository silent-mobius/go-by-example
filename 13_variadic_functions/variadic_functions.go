package main

import "fmt"

func sum(nums ...int) {
	fmt.Print(nums, " ")
	total := 0

	for _, n := range nums {

		total += n
	}
	fmt.Println(total)
	//return total
}

func main() {
	sum(2, 4)
	sum(1, 2, 3)

	nums := []int{1, 2, 3, 4}
	sum(nums...)
}
