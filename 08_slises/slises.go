package main 

import "fmt"

func main(){

	s :=make([]string, 3)
	fmt.Println("empty: ", s)

	s[0]="a"
	s[1]="b"
	s[2]="c"
	fmt.Println("set:", s)
	fmt.Println("get:", s[2])

	fmt.Println("len: ",len(s))
	s = append(s,"d")
	s = append(s, "e","f")
	fmt.Println("append:" , s)


	c := make([]string, len(s))
	copy(c,s)
	fmt.Println("copy:", c)

	l := s[2:5]
	fmt.Println("sliseof :", l)
	
	t := []string{"g","h", "i"}
	fmt.Println("declare: ", t)

	two_dimention_slise := make([][]int,3)
	for i:=0; i<3; i++ {
		inner_len := i + 1
		two_dimention_slise[i]=make([]int, inner_len)
		for j:=0; j< inner_len; j++{
			two_dimention_slise[i][j] =  i+j
		}
	}
	fmt.Println("2D:" , two_dimention_slise)


}
